# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a repository for EduViz, HW 5 for CSE401 Autumn 2016. The project contributors are Shiv Ahluwalia and Dang Le.

### Document Links ###

Pre-Proposal Doc: https://docs.google.com/document/d/1k2Hm7KdrpyQ4rfmvDoGkVLlYH6lXvHWHyZZHTzn4DCU/edit?usp=sharing

Proposal Doc: https://docs.google.com/document/d/1nLz-PR0V1mMahS5PmvC6KMPlwP24R9iEeeWxLkOn0Uo/edit?usp=sharing

Design: https://docs.google.com/document/d/1FDPxtrOd3wTaM4pmYk8KasQci76hSaEVuzuP9rMRugk/edit

Report: https://docs.google.com/document/d/1u08ey6xPTWjJQkOOag7MB_CMYx-DmaJyrYKx819N5zU/edit

Posters: https://docs.google.com/presentation/d/1iDuMOvTDTGLWaxgkahQpUw9CTy_bUhvq6HgIVU-d5Hk/edit#slide=id.g1aa19a84eb_0_155

Screencast demo: https://youtu.be/i8Qd95QW7O4

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
