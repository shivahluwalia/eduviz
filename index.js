// custom variables
var w = 500, h = 200,
    num = 0, speed = 100,
    choice = "selection",
    array, scale, padding = 2, timer,
    states = {"default": 0, "finished": 1, "current": 2, "compare": 3, "minimal": 4, "hide": 5},
    colors = ["#B7C4CF", "#3565A1", "#D55511", "#74A82A", "#A42F11", "#fff"],
    color_default = "#6A6BCD", color_highlight = "#C24787",
    svg;

// init the graph
getArray();
getGraph(array);

// algo definition
var algos = {};

algos.selection = function() {
    var i, j, len = array.length, minimum, flag = false;
    
    j = 0; i = len; flag = false;
    
    timer = setInterval(function() {
        
        if (j < len - 1) {
            array[j].state = states.current;
            
            if (!flag) {
                minimum = j;
                i = j + 1;
                flag = true;
            }
            
            if (i < len) {
                if (array[i - 1].state === states.compare) {
                    array[i - 1].state = states.default;
                }
                
                array[i].state = states.compare;
                
                if (array[i].num < array[minimum].num) {
                    if (minimum !== j)
                        array[minimum].state = states.default;
                    
                    minimum = i;
                    
                    array[minimum].state = states.minimal;
                }
                
                i++;
            } else {
                flag = false;
                
                if (minimum !== j) {
                    swap(minimum, j);
                }
                
                array[i - 1].state = states.default;
                array[minimum].state = states.default;
                array[j].state = states.finished;
                
                j++;
            }
        } else {
            array[j].state = states.finished;
            clearInterval(timer);
        }
        
        reDraw(array);
        
    }, speed);
};

algos.insertion = function() {
    var i, j, len = array.length, flag = false;
    
    j = 1; i = 0; flag = false;
    
    array[0].state = states.finished;
    // Timer makes the sorting goes slow and easy to see
    timer = setInterval(function() {
        // Sorting
        // states keeps track of the state at the index
        if (j < len) {
            if (!flag) {
                array[j].state = states.hide;
                
                i = j - 1;
                flag = true;
            }
            
            if (i >= 0) {
                if (array[i].num > array[i + 1].num) {
                    swap(i, i + 1);
                } else {
                    array[i + 1].state = states.finished;
                    
                    j++;
                    flag = false;
                }
                
                i--;
            } else {
                array[0].state = states.finished;
                
                j++;
                flag = false;
            }
            
        } else {
            clearInterval(timer);
        }
        // Have to redraw each time the array change
        reDraw(array);
        
    }, speed);
};

algos.bubble = function() {
    var i, j, len = array.length, flag = false, wait;
    
    j = len - 1; i = 0; flag = false; wait = false;
    
    timer = setInterval(function() {
        
        if (j > 0) {
            if (!flag) {
                i = 0;
                flag = true;
            }
            
            if (i < j) {
                if (!wait) {
                    if (i != 0) {
                        array[i - 1].state = states.default;
                    }
                    
                    array[i].state = states.current;
                    array[i + 1].state = states.compare;
                } else {
                    if (array[i].num > array[i + 1].num) {
                        swap(i, i + 1);
                    }
                    
                    i++;
                }
                
                wait = !wait;
            } else {
                array[j - 1].state = states.default;
                array[j].state = states.finished;
                
                j--;
                flag = false;
            }
            
        } else {
            array[0].state = states.finished;
            
            clearInterval(timer);
        }
        
        reDraw(array);
        
    }, speed);
};

// swap function
function swap(i, j) {
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
}

// generate random dataset
function getArray() {
    var str = document.getElementById("numbers").value;

/*
    var str;
    if (document.getElementById('numbers') != null) {
        str = document.getElementById("numbers").value;
    }
    else {
        str = "1,2,3,4,5";
    }
*/
    if (str.substring(0,7) === "default"){
        var start = str.indexOf("(");
        var end = str.indexOf(")");
        var elementCount = parseFloat(str.substring(start + 1, end));
        array = [];
        var i = 0;
        for(; i < elementCount; i++){
            array[i] = { num: elementCount - i, state: states.default };
        }
        var j = elementCount - 1;
        for (; j > 0; j--){
            var rand = Math.floor((Math.random() * elementCount) + 1);
            var temp = array[j].num;
            array[j].num = array[rand].num;
            array[rand].num = temp;
        }

    }
    else{
        var splitNum = str.split(",");
        for (e in splitNum) {
            splitNum[e] = parseFloat(splitNum[e]);
        }
        num = splitNum.length;

        var i = 0;
    
        array = [];

        for (; i < num; i++) {
            array[i] = { num: splitNum[i], state: states.default };
        }
    }

    scale = d3.scale.linear()
            .domain([0, d3.max(array, function(d) { return d.num; })])
            .range([3, h]);
}

// create rect in svg
function getGraph(set) {
    document.getElementById("graph").innerHTML = "";
    
    svg = d3.select("#graph")
            .append("svg")
            .attr("width", w)
            .attr("height", h);
    
    var bars = svg.selectAll("rect")
                    .data(set)
                    .enter()
                    .append("rect");
    
    bars.attr("x", function(d, i) {
        return i * (w / set.length);
    });
    
    bars.attr("y", function(d, i) {
        return h - scale(d.num);
    });
    
    bars.attr("width", function(d, i) {
        return (w / set.length) - padding;
    });
    
    bars.attr("height", function(d, i) {
        return scale(d.num);
    });
    
    bars.attr("fill", function(d, i) {
        return colors[d.state];
    });
}

// redraw
function reDraw(set) {
    var bars = svg.selectAll("rect")
                    .data(set)
                    .transition()
                    .duration(speed / 2 | 0)
    
    bars.attr("y", function(d, i) {
        return h - scale(d.num);
    });
    
    bars.attr("width", function(d, i) {
        return (w / set.length) - padding;
    });
    
    bars.attr("height", function(d, i) {
        return scale(d.num);
    });
    
    bars.attr("fill", function(d, i) {
        return colors[d.state];
    });
}

// selection change
document.getElementById("choice").addEventListener("change", function() {
    choice = this.value;
});

// reset
document.getElementById("reset").addEventListener("click", function() {
    clearInterval(timer);
    getArray();
    getGraph(array);
});

// play
document.getElementById("play").addEventListener("click", function() {
    clearInterval(timer);
    
    for (var i = array.length - 1; i >= 0; i--)
        array[i].state = states.default;
    
    algos[choice]();
});
